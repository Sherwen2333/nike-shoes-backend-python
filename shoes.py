import random

shoe_database = [
    {
        'shoeId': '1',
        'model': 'Nike Air Max 95 SE',
        'minPrice': 120,
        'maxPrice': 150,
    },
    {
        'shoeId': '2',
        'model': 'Nike Air Max 97 SE',
        'minPrice': 50,
        'maxPrice': 150,
    },
    {
        'shoeId': '3',
        'model': 'Nike Air Max Pre-Day',
        'minPrice': 120,
        'maxPrice': 160,
    },
    {
        'shoeId': '4',
        'model': 'Nike Air Max 270',
        'minPrice': 100,
        'maxPrice': 130,
    },
    {
        'shoeId': '5',
        'model': 'Nike Renew Ride 3',
        'minPrice': 180,
        'maxPrice': 200,
    },
    {
        'shoeId': '6',
        'model': 'Nike Air Max 90',
        'minPrice': 120,
        'maxPrice': 150,
    },
]


def get_shoe_price(shoe_data):
    return shoe_data['minPrice'] + random.randint(-shoe_data['minPrice'] + 1, shoe_data['maxPrice'])


def get_shoe_info(shoe_data):
    return shoe_data['']
