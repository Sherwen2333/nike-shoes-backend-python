import flask
from flask import jsonify

from flask_cors import CORS

import shoes
from shoes import shoe_database

app = flask.Flask(__name__)
app.config["DEBUG"] = True
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


# @cross_origin()
@app.route('/api/shoe-price', methods=['GET`'])
def fetch_shoe_price():
    ret = []
    for each in shoe_database:
        each.update({'shoePrice': shoes.get_shoe_price(each)})
        ret.append(each)
    ret[-1]['shoePrice'] = -100
    return jsonify(ret)


app.run(host="localhost", port=8081, debug=True)
