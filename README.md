### Nike Interview Backend Code Challenge [Python]

_The provided code document should contain more details._

Start server (Port 8081): `python api.py` (you need to install `python`, `pip` & `flask` on your machine)

APIs:

1. Clone [this project](https://gitlab.com/hiring_nike_china/fetch-shoe-prices/) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```

### Change & Code improvements 

1. I added cros policy in order for debug info(I used the server and frontend on a different port initally)
2. I put the `shoe_database` from the frontend to separate file in the backend
3. I changed the api a little bit from `/api/shoe-price/{id}` to `/api/shoe-price` 

### If you had more time, what would you add further.

1. I would use a relational database to store all the shoe data
2. I would host the website online instead of localhost

### What were your doubts, and what were your assumptions for the projects?

1. I thought I was going to write everything.
2. I was thinking of industry level of code was given and I would have to find bugs.

### Any other notes, that are relevant to your task.

1. I was originally want to use spring boot as the backend but somehow I was not able to load the project correctly. My last time using spring framework was way to college senior year. So I taught myself flask by some youtube videos in order to get the server running. 
